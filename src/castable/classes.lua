--
-- classes.lua
-- Created by: Jérémy "Vrakfall" Lecocq
--
-- Classes for Weak Auras that have special animation functions.
-- All classes are closure-based for faster access (but it uses more memory for each item.
-- This has to be put in a special `wa` that is going to be used as a dependency by the other ones. TODO: Check if that works.
--

-- Constants

-- WeakAuras2 conventions

VWA = {}
VWA.MAX_PROGRESS = 1

-- Auras

VWA.DEFAULT_AURA_STACK_AMOUNT = 1

-- - Alpha

VWA.NOT_IN_RANGE_ALPHA = 0.2
VWA.NO_TARGET_ALPHA = 0
VWA.IN_RANGE_LOW_ALPHA = 0.4
VWA.IN_RANGE_HIGH_ALPHA = 1

-- - Timings

-- TODO: Update the GCD more often
--_,VWA.GCD = UnitPowerType("player") == 3 and (WA.GetUnitBuff("player",13750) and .8 or 1) or max(1.5/(1 + .01 * UnitSpellHaste("player")), WA_GetUnitBuff("player", 194249) and .67 or .75)

-- - Pop Animation

VWA.POP_DURATION = 0.2
VWA.LEAD = VWA.POP_DURATION
VWA.FALLBACK_PROGRESS_TIME_TO_START_POP_ANIMATION = 1 - 0.05
VWA.FALLBACK_PROGRESS_TIME_TO_REVERSE_POP_ANIMATION = 1 - 0.025
VWA.FALLBACK_PROGRESS_TIME_TO_END_POP_ANIMATION = 1
VWA.POP_MULTIPLIER = 1

-- Utils -- Utility methods

-- TODO: Move this to a file dedicated to utility functions.
--- OverwriteObject(object1, object2) -- Overwrites the `object2` into `object1`. Used for multiple-inheritance.
local function OverwriteObject(object1, object2)
	local newObject = object1

	for i, v in pairs(object2) do
		newObject[i] = v
	end
end

-- TODO: Move this a file dedicated to utility functions.
local function SetIfNotNil(variableToOverwrite, variableToSet)
	if variableToSet == nil then
		return variableToOverwrite
	end

	return variableToSet
end

-- Spell -- The base class for all the spells.
VWA.Spell = function(name, isTrinket)
	local self = {}

	local _name = name
	-- Can either be the duration of an aura or the cooldown of a normal spell.
	local _duration = 0

	local _isTrinket = SetIfNotNil(false, isTrinket)

	self.GetName = function()
		return _name
	end

	self.GetDuration = function()
		return _duration
	end

	self.SetDuration = function(duration)
		local _,gcd = GetSpellCooldown(61304)
		if duration >= 0 and duration ~= gcd then
			_duration = duration
		end
	end

	self.IsInRange = function(target)
		if _isTrinket then
			return IsSpellInRange("Penance", target) --TODO: Make this better
		end
		return IsSpellInRange(name, target)
	end

	self.CooldownTimeLeft = function()
		local start, duration
		if _isTrinket then
			start, duration = GetItemCooldown(GetInventoryItemID("player", 13)) --TODO: Make this better
		else
			start, duration, _, _ = GetSpellCooldown(name)
		end

		self.SetDuration(duration)

		local endTime = start + duration
		local cooldown = endTime - GetTime()

		local _,gcd = GetSpellCooldown(61304)

		if cooldown > 0 then
			return cooldown, duration == gcd
		end

		return 0, false
		end

	self.IsOnCooldown = function()
		local timeLeft, onGCD = self.CooldownTimeLeft()

		return timeLeft > 0 and not onGCD
	end

	-- To close the gap with non-aura spells, so it can be used on both.
	self.ShouldBeUsed = function(_)
		return not self.IsOnCooldown()
	end

	return self
end

-- Aura -- The base class for dot/hot spells (from the player itself, so far, to be changed). Inherits from `Spell`.
VWA.Aura = function(name, desiredStackAmount)
	local self = VWA.Spell(name)

	local _desiredStackAmount = SetIfNotNil(VWA.DEFAULT_AURA_STACK_AMOUNT, desiredStackAmount)

	local _lastTargetChecked
	local _lastTargetCheckedTime

	-- Should be overwritten if `CheckTarget` and `stacksOnTarget` are.
	local _stacksOnTargetCache

	self.getDesiredStackAmount = function()
		return _desiredStackAmount
	end

	self.getStacksOnTargetCache = function()
		return _stacksOnTargetCache
	end

	self.setStacksOnTargetCache = function(stacksOnTargetCache)
		if stacksOnTargetCache >= 0 then
			_stacksOnTargetCache = stacksOnTargetCache
		end
	end

	self.IsTargetAlreadyChecked = function(target)
		if target == _lastTargetChecked and _lastTargetCheckedTime == GetTime() then
			return true
		end
		return false
	end

	-- Checks if the aura is on the target then put what is found in `_stacksOnTargetCache`. !! -> This should be overwritten by subclasses that know if the spell is a buff or a debuff (for a faster API call).
	self.CheckTarget = function(target, isBuff)
		for i=1,40 do
			local auraName, stackAmount, duration
			if isBuff then
				auraName,_,stackAmount,_,duration = UnitBuff(target, i, "PLAYER")
			else
				auraName,_,stackAmount,_,duration = UnitDebuff(target, i, "PLAYER")
			end

			if auraName == nil then -- The end of the debuff array has been reached
				self.setStacksOnTargetCache(0)
				return 0
			end

			if auraName == self.GetName() then
				if stackAmount == 0 then
					stackAmount = 1
				end

				self.setStacksOnTargetCache(stackAmount)
				self.SetDuration(duration)
				return stackAmount
			end
		end

		self.setStacksOnTargetCache(0)
		return 0
	end

	-- Checks if the aura is on the target and returns its stack amount. If the value is already cached, it just returns it. Otherwise, it first calls the check which is going to put the value in cache. !! -> This should be overwritten by subclasses that know if the spell is a buff or a debuff (for a faster API call).
	self.stacksOnTarget = function(target)
		if self.IsTargetAlreadyChecked(target) then
			return getStacksOnTargetCache
		end

		if self.CheckTarget(target, true) == 0 then
			return self.CheckTarget(target, false)
		end

		return getStacksOnTargetCache
	end

	self.neededStacks = function(target)
		return self.getDesiredStackAmount() - self.stacksOnTarget(target)
	end

	-- To close the gap with non-aura spells, so it can be used on both.
	self.ShouldBeUsed = function(target)
		if self.GetName() == "Unstable Affliction" then
			print(self.stacksOnTarget(target))
		end

		return self.stacksOnTarget(target) < _desiredStackAmount
	end

	return self
end

VWA.Debuff = function(name, desiredStackAmount)
	local self = VWA.Aura(name, desiredStackAmount)

	-- Overwrite
	self.CheckTarget = function(target)
		for i=1,40 do
			local auraName,_,stackAmount,_,duration = UnitDebuff(target, i, "PLAYER")

			if auraName == nil then -- The end of the debuff array has been reached
				self.setStacksOnTargetCache(0)
				return 0
			end

			if auraName == self.GetName() then
				if stackAmount == 0 then
					stackAmount = 1
				end

				self.setStacksOnTargetCache(stackAmount)
				self.SetDuration(duration)
				return stackAmount
			end
		end

		self.setStacksOnTargetCache(0)
		return 0
	end

	-- Overwrite
	self.stacksOnTarget = function(target)
		if self.IsTargetAlreadyChecked(target) then
			return self.getStacksOnTargetCache()
		end

		return self.CheckTarget(target)
	end

	return self
end

VWA.SeparatelyStackedDebuff = function(name, desiredStackAmount)
	local self = VWA.Debuff(name, desiredStackAmount)

	-- Overwrite
	self.CheckTarget = function(target)
		local totalStackAmount = 0

		for i=1,40 do
			local auraName,_,stackAmount,_,duration = UnitDebuff(target, i, "PLAYER")

			if auraName == nil then -- The end of the debuff array has been reached
				break
			end

			if auraName == self.GetName() then
				if stackAmount == 0 then
					stackAmount = 1
				end

				totalStackAmount = totalStackAmount + stackAmount
				self.SetDuration(duration)
			end
		end

		self.setStacksOnTargetCache(totalStackAmount)
		return totalStackAmount
	end

	return self
end

VWA.GrowingThenPopAnimation = function(spell, target, lead, popDuration, popMultiplier, notInRangeAlpha, noTargetAlpha, inRangeLowAlpha, inRangeHighAlpha)
	local self = {}

	local _lead = VWA.LEAD
	_lead = SetIfNotNil(_lead, lead)

	local _popDuration = VWA.POP_DURATION
	_popDuration = SetIfNotNil(_popDuration, popDuration)

	local _popMultiplier = VWA.POP_MULTIPLIER
	_popMultiplier = SetIfNotNil(_popMultiplier, popMultiplier)

	local _notInRangeAlpha = VWA.NOT_IN_RANGE_ALPHA
	_notInRangeAlpha = SetIfNotNil(_notInRangeAlpha, notInRangeAlpha)

	local _noTargetAlpha = VWA.NO_TARGET_ALPHA
	_noTargetAlpha = SetIfNotNil(_noTargetAlpha, noTargetAlpha)

	local _inRangeLowAlpha = VWA.IN_RANGE_LOW_ALPHA
	_inRangeLowAlpha = SetIfNotNil(_inRangeLowAlpha, inRangeLowAlpha)

	local _inRangeHighAlpha = VWA.IN_RANGE_HIGH_ALPHA
	_inRangeHighAlpha = SetIfNotNil(_inRangeHighAlpha, inRangeHighAlpha)

	self.ProgressTimeToStartPopAnimation = function()
		if spell.GetDuration() > 0 then
			return 1 - _lead / spell.GetDuration()
		end
		return VWA.FALLBACK_PROGRESS_TIME_TO_START_POP_ANIMATION
	end

	self.ProgressTimeToReversePopAnimation = function()
		if spell.GetDuration() > 0 then
			return 1 - ( _lead - _popDuration / 2 ) / spell.GetDuration()
		end
		return VWA.FALLBACK_PROGRESS_TIME_TO_REVERSE_POP_ANIMATION
	end

	self.ProgressTimeToEndAnimation = function()
		if spell.GetDuration() > 0 then
			return 1 - ( _lead - _popDuration ) / spell.GetDuration()
		end
		return VWA.FALLBACK_PROGRESS_TIME_TO_END_POP_ANIMATION
	end

	self.GetAlphaFunction = function(progress, start, delta)
		-- TODO: Implement with `start` and `delta`.
		local isInRange = spell.IsInRange(target)

		if isInRange == 1 then -- If there's an enemy target and it's in range
			if spell.ShouldBeUsed("target") or progress >= self.ProgressTimeToStartPopAnimation() then -- If it's time to use the spell (The spell isn't on the target or the pop animation has started). This only works if the animation starts before and ends before or at the same frame the progress ends.
				return _inRangeHighAlpha
			elseif progress == 0 then
				return 0
			else
				return _inRangeLowAlpha
			end
		else
			if isInRange == nil then -- If no target is selected
				return _noTargetAlpha
			else
				return _notInRangeAlpha
			end
		end
	end

	self.GetZoomFunction = function(progress, startX, startY, scaleX, scaleY)
		-- TODO: Implement with `startX`, `starY`, `scaleX`, `scaleY`.

		if not spell.ShouldBeUsed(target) then -- This only works if the animation starts before and ends before or at the same frame the progress ends.

			if progress == 0 then
				return 0, 0 -- This makes so auras without end like "Corruption" with the "Absolute Corruption" talent aren't visible when applied.

			elseif progress < self.ProgressTimeToStartPopAnimation() then
				return progress, progress

			elseif progress < self.ProgressTimeToReversePopAnimation() then

				local animMultiplier = (progress - self.ProgressTimeToStartPopAnimation()) / (self.ProgressTimeToReversePopAnimation() - self.ProgressTimeToStartPopAnimation())
				return 1 + animMultiplier * _popMultiplier, 1 + animMultiplier * _popMultiplier

			elseif progress < self.ProgressTimeToEndAnimation() then

				local animMultiplier = 1 - (progress - self.ProgressTimeToReversePopAnimation()) / (self.ProgressTimeToEndAnimation() - self.ProgressTimeToReversePopAnimation())
				return 1 + animMultiplier * _popMultiplier, 1 + animMultiplier * _popMultiplier

			end
		end

		-- In every other case, just return `1, 1` (normal size).
		return 1, 1
	end

	return self
end

print("VWA loaded.")
