--
-- oldAgony.lua
-- Created by: Jérémy "Vrakfall" Lecocq
--
-- Agony animation, how it was made before.
-- This is for the Warlock's (Affliction) spell Agony.
--

-- Actions | On Init

local isPopAnimDone = true
local isPopAnimStarted = false
local popAnimStart = 0
local popAnimMid = 0
local popAnimEnd = 0
-- The lead the pop animation should have. Useful to fight lag or in use with
-- dots.
local popAnimLead = 3.2

function SettleAgonyPopAnimation(time, duration)
	SetAgonyPopAnimStarted(true)
	SetAgonyPopAnimStart(time)
	SetAgonyPopAnimMid(GetAgonyPopAnimStart() + duration / 2)
	SetAgonyPopAnimEnd(GetAgonyPopAnimStart() + duration)
end

-- Getters and Setters

function IsAgonyPopAnimDone()
	return isPopAnimDone
end

function SetAgonyPopAnimDone(isPAD)
	if isPAD == true or isPAD == false then
		isPopAnimDone = isPAD
	end
end

function IsAgonyPopAnimStarted()
	return isPopAnimStarted
end

function SetAgonyPopAnimStarted(isPAS)
	if isPAS == true or isPAS == false then
		isPopAnimStarted = isPAS
	end
end

function GetAgonyPopAnimStart()
	return popAnimStart
end

function SetAgonyPopAnimStart(pAS)
	if pAS > 0 then
		popAnimStart = pAS
	end
end

function GetAgonyPopAnimMid()
	return popAnimMid
end

function SetAgonyPopAnimMid(pAM)
	if pAM > 0 then
		popAnimMid = pAM
	end
end

function GetAgonyPopAnimEnd()
	return popAnimEnd
end

function SetAgonyPopAnimEnd(pAE)
	if pAE > 0 then
		popAnimEnd = pAE
	end
end

function GetPopAnimLead()
	return popAnimLead
end

-- Animations | Main | Fade
local fade = -- Leave this, it helps IntelliJ's parsing because this is how the function is used by `Weak Auras`
-- Copy only the following

function(_,_,_)
	local isInRange = IsSpellInRange("Agony", target)
	local lowAlpha = 0.2
	local lowAlphaInRange = 0.4
	-- local start = 1 --Because the argument start (the second one) doesn't seem to work
	--TODO: Look at what I wanted to do with that
	-- local alphaDiff = start + delta

	for i=1,40 do
		local name,_,_,_,_,endT = UnitDebuff("target", i, "PLAYER")

		if name == nil then
			break
		end

		if name == "Agony" then
			local fakeEnd = endT - GetPopAnimLead()
			local left = fakeEnd - GetTime()

			if isInRange == 1 then
				-- If in range to cast
				if left <= 0 then
					-- return 1 * start
					return 1
				else
					-- return (lowAlpha + alphaDiff) * start
					return lowAlphaInRange
				end
			else
				-- If not in range to cast
				if isInRange == 0 then
					-- return lowAlpha * start
					return lowAlpha
				else
					-- return 0 * start
					return 0
				end
			end
		end
	end

	-- If there's no debuff applied
	if isInRange == 1 then
		-- If the target is in range, full alpha
		return 1
	else
		if isInRange == 0 then
			-- If the target is in range, low alpha
			return lowAlpha
		else
			-- If there's no target, no alpha
			return 0
		end
	end
end

-- Animations | Main | Zoom
local zoom = -- Leave this, it helps IntelliJ's parsing because this is how the function is used by `Weak Auras`
-- Copy only the following

function(_,_,_,scaleX, scaleY)
	local time = GetTime()

	local popAnimDuration = 0.2

	for i=1,40 do
		local name,_,_,_,cd,endT = UnitDebuff("target", i, "PLAYER")

		if name == nil then
			break
		end

		if name == "Agony" then
			local start = endT - cd

			local fakeEnd = endT - GetPopAnimLead()
			local left = fakeEnd - time

			if left <= 0 then
				cd = 0
			end

			local progress = (time - start) / (cd - GetPopAnimLead())
			-- Debug output
			-- print("start: " .. start)
			-- print("cd: " .. cd)
			-- print("left: " .. left)
			-- print("progress: " .. progress)

			if cd > 0 then
				SetAgonyPopAnimDone(false)
				return progress * 1, progress * 1
			else
				if IsAgonyPopAnimDone() then
					return 1, 1
				else
					if not IsAgonyPopAnimStarted() then
						SettleAgonyPopAnimation(time, popAnimDuration)
					end

					if time > GetAgonyPopAnimEnd() then
						SetAgonyPopAnimDone(true)
						SetAgonyPopAnimStarted(false)
						return 1, 1
					else
						if time > GetAgonyPopAnimMid() then
							-- Pop retraction animation
							local animMultiplier = (GetAgonyPopAnimEnd() - time) / (popAnimDuration / 2)
							return 1 + scaleX * animMultiplier, 1 + scaleY * animMultiplier
						else
							-- Pop expansion animation
							local animMultiplier = (time - GetAgonyPopAnimStart()) / (popAnimDuration / 2)
							return 1 + scaleX * animMultiplier, 1 + scaleY * animMultiplier
						end
					end
				end
			end
		end
	end
end
