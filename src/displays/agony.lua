--
-- agony.lua
-- Created by: Jérémy "Vrakfall" Lecocq
--

-- Actions - On init:

agony = VWA.Debuff("Agony")
agonyAnim = VWA.GrowingThenPopAnimation(agony, "target", 3)

-- Animations - Fade:

function(progress, start, delta)
	return agonyAnim.GetAlphaFunction(progress, start, delta)
end

-- Animations - Zoom:

function(progress, startX, startY, scaleX, scaleY)
	return agonyAnim.GetZoomFunction(progress, startX, startY, scaleX, scaleY)
end
