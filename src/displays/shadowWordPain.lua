---
--- shadowWordPain.lua
--- Created by: Jérémy "Vrakfall" Lecocq
---

-- Actions - On init:

aura_env.spell = VWA.Debuff("Shadow Word: Pain")
aura_env.anim = VWA.GrowingThenPopAnimation(aura_env.spell, "target")

-- Animations - Fade:

function(progress, start, delta)
	return aura_env.anim.GetAlphaFunction(progress, start, delta)
end

-- Animations - Zoom:

function(progress, startX, startY, scaleX, scaleY)
	return aura_env.anim.GetZoomFunction(progress, startX, startY, scaleX, scaleY)
end
