---
--- unstableAffliction.lua
--- Created by: Jérémy "Vrakfall" Lecocq
---

-- Actions - On init:

aura_env.spell = VWA.SeparatelyStackedDebuff("Unstable Affliction", 5)
aura_env.anim = VWA.GrowingThenPopAnimation(aura_env.spell, "target")

-- Display - Text

function()
	return math.min(UnitPower("player", 7, true)*0.1, aura_env.spell.neededStacks("target"))
end

-- Animations - Fade:

function(progress, start, delta)
	return aura_env.anim.GetAlphaFunction(progress, start, delta)
end

-- Animations - Zoom:

function(progress, startX, startY, scaleX, scaleY)
	return aura_env.anim.GetZoomFunction(progress, startX, startY, scaleX, scaleY)
end
