--
-- phantomSingularity.lua
-- Created by: Jérémy "Vrakfall" Lecocq
--

-- Actions - On init:

aura_env.phantomSingularity = VWA.Spell("Phantom Singularity")
aura_env.phantomSingularityAnim = VWA.GrowingThenPopAnimation(aura_env.phantomSingularity, "target")

-- Animations - Fade:

function(progress, start, delta)
	return aura_env.phantomSingularityAnim.GetAlphaFunction(progress, start, delta)
end

-- Animations - Zoom:

function(progress, startX, startY, scaleX, scaleY)
	return aura_env.phantomSingularityAnim.GetZoomFunction(progress, startX, startY, scaleX, scaleY)
end
